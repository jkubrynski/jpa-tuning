package com.kubrynski.jpa.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;

@Entity
public class Company extends BaseEntity {

	private String name;

	@OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, fetch = FetchType.LAZY)
	@LazyToOne(LazyToOneOption.NO_PROXY)
	private Subscription subscription;

	protected Company() {
	}

	public Company(String name, Subscription subscription) {
		this.name = name;
		this.subscription = subscription;
	}

	public Subscription getSubscription() {
		return subscription;
	}

	@Basic(fetch = FetchType.LAZY)
	@Access(AccessType.PROPERTY)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSubscription(Subscription subscription) {
		this.subscription = subscription;
	}

}
