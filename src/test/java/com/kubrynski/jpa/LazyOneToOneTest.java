package com.kubrynski.jpa;

import com.kubrynski.jpa.model.Company;
import com.kubrynski.jpa.model.Subscription;
import org.junit.Test;

public class LazyOneToOneTest extends BaseJpaTest {

	@Test
	public void contextLoads() {
		doInTransaction(() -> em.persist(new Company("Company", new Subscription())));
		doInTransaction(() -> {
			LOG.info("Loading company");
			Company company = em.find(Company.class, 1L);
			LOG.info("Getting Subscription ID");
			Long id = company.getSubscription().getId();
			LOG.info("Subscription ID : {}", id);
			LOG.info("Subscription class name: {}", company.getSubscription().getClass().getCanonicalName());
		});
	}

}
